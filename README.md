Jerba's Sprite Sheet Animation Tester

Simple tool to view Sprite Sheets Animations.
Supports .png, .jpg, .gif, .bmp, .psd, .svg files.