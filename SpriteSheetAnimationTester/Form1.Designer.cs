﻿namespace SpriteSheetAnimationTester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.loadImage = new System.Windows.Forms.Button();
            this.textWidth = new System.Windows.Forms.TextBox();
            this.textHeight = new System.Windows.Forms.TextBox();
            this.textRows = new System.Windows.Forms.TextBox();
            this.textColumns = new System.Windows.Forms.TextBox();
            this.textDurMs = new System.Windows.Forms.TextBox();
            this.textFps = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textZoom = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonStartStop = new System.Windows.Forms.Button();
            this.viewSheet = new System.Windows.Forms.CheckBox();
            this.renderingWindow = new SpriteSheetAnimationTester.DrawTest();
            this.SuspendLayout();
            // 
            // loadImage
            // 
            this.loadImage.Location = new System.Drawing.Point(12, 12);
            this.loadImage.Name = "loadImage";
            this.loadImage.Size = new System.Drawing.Size(108, 23);
            this.loadImage.TabIndex = 1;
            this.loadImage.Text = "Load Image";
            this.loadImage.UseVisualStyleBackColor = true;
            this.loadImage.Click += new System.EventHandler(this.loadImage_Click);
            // 
            // textWidth
            // 
            this.textWidth.Location = new System.Drawing.Point(12, 58);
            this.textWidth.Name = "textWidth";
            this.textWidth.Size = new System.Drawing.Size(51, 20);
            this.textWidth.TabIndex = 2;
            this.textWidth.TextChanged += new System.EventHandler(this.textWidth_TextChanged);
            // 
            // textHeight
            // 
            this.textHeight.Location = new System.Drawing.Point(69, 58);
            this.textHeight.Name = "textHeight";
            this.textHeight.Size = new System.Drawing.Size(51, 20);
            this.textHeight.TabIndex = 3;
            this.textHeight.TextChanged += new System.EventHandler(this.textHeight_TextChanged);
            // 
            // textRows
            // 
            this.textRows.Location = new System.Drawing.Point(12, 98);
            this.textRows.Name = "textRows";
            this.textRows.Size = new System.Drawing.Size(51, 20);
            this.textRows.TabIndex = 4;
            this.textRows.TextChanged += new System.EventHandler(this.textRows_TextChanged);
            // 
            // textColumns
            // 
            this.textColumns.Location = new System.Drawing.Point(69, 98);
            this.textColumns.Name = "textColumns";
            this.textColumns.Size = new System.Drawing.Size(51, 20);
            this.textColumns.TabIndex = 5;
            this.textColumns.TextChanged += new System.EventHandler(this.textColumns_TextChanged);
            // 
            // textDurMs
            // 
            this.textDurMs.Location = new System.Drawing.Point(12, 138);
            this.textDurMs.Name = "textDurMs";
            this.textDurMs.Size = new System.Drawing.Size(51, 20);
            this.textDurMs.TabIndex = 6;
            this.textDurMs.TextChanged += new System.EventHandler(this.textDurMs_TextChanged);
            // 
            // textFps
            // 
            this.textFps.Location = new System.Drawing.Point(69, 138);
            this.textFps.Name = "textFps";
            this.textFps.Size = new System.Drawing.Size(51, 20);
            this.textFps.TabIndex = 7;
            this.textFps.TextChanged += new System.EventHandler(this.textFps_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Width";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Height";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Rows";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(69, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Columns";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Dur (ms)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(69, 122);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "FPS";
            // 
            // textZoom
            // 
            this.textZoom.Location = new System.Drawing.Point(12, 178);
            this.textZoom.Name = "textZoom";
            this.textZoom.Size = new System.Drawing.Size(51, 20);
            this.textZoom.TabIndex = 14;
            this.textZoom.TextChanged += new System.EventHandler(this.textZoom_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 162);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Zoom";
            // 
            // buttonStartStop
            // 
            this.buttonStartStop.Location = new System.Drawing.Point(13, 205);
            this.buttonStartStop.Name = "buttonStartStop";
            this.buttonStartStop.Size = new System.Drawing.Size(107, 23);
            this.buttonStartStop.TabIndex = 16;
            this.buttonStartStop.Text = "Play";
            this.buttonStartStop.UseVisualStyleBackColor = true;
            this.buttonStartStop.Click += new System.EventHandler(this.buttonStartStop_Click);
            // 
            // viewSheet
            // 
            this.viewSheet.AutoSize = true;
            this.viewSheet.Location = new System.Drawing.Point(13, 234);
            this.viewSheet.Name = "viewSheet";
            this.viewSheet.Size = new System.Drawing.Size(110, 17);
            this.viewSheet.TabIndex = 17;
            this.viewSheet.Text = "View Sprite Sheet";
            this.viewSheet.UseVisualStyleBackColor = true;
            this.viewSheet.CheckedChanged += new System.EventHandler(this.viewSheet_CheckedChanged);
            // 
            // renderingWindow
            // 
            this.renderingWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.renderingWindow.BackColor = System.Drawing.Color.White;
            this.renderingWindow.columns = 0;
            this.renderingWindow.duration = 0F;
            this.renderingWindow.height = 0;
            this.renderingWindow.Location = new System.Drawing.Point(139, 11);
            this.renderingWindow.Name = "renderingWindow";
            this.renderingWindow.playing = false;
            this.renderingWindow.rows = 0;
            this.renderingWindow.Size = new System.Drawing.Size(512, 512);
            this.renderingWindow.TabIndex = 0;
            this.renderingWindow.Text = "r";
            this.renderingWindow.viewSheet = false;
            this.renderingWindow.width = 0;
            this.renderingWindow.zoom = 0F;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(663, 535);
            this.Controls.Add(this.viewSheet);
            this.Controls.Add(this.buttonStartStop);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textZoom);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textFps);
            this.Controls.Add(this.textDurMs);
            this.Controls.Add(this.textColumns);
            this.Controls.Add(this.textRows);
            this.Controls.Add(this.textHeight);
            this.Controls.Add(this.textWidth);
            this.Controls.Add(this.loadImage);
            this.Controls.Add(this.renderingWindow);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(679, 574);
            this.Name = "Form1";
            this.Text = "Jerba\'s Sprite Sheet Animation Tester";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DrawTest renderingWindow;
        private System.Windows.Forms.Button loadImage;
        private System.Windows.Forms.TextBox textWidth;
        private System.Windows.Forms.TextBox textHeight;
        private System.Windows.Forms.TextBox textRows;
        private System.Windows.Forms.TextBox textColumns;
        private System.Windows.Forms.TextBox textDurMs;
        private System.Windows.Forms.TextBox textFps;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textZoom;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonStartStop;
        private System.Windows.Forms.CheckBox viewSheet;
    }
}

