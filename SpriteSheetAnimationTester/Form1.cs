﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimplePsd;
using Svg;

namespace SpriteSheetAnimationTester
{
    public partial class Form1 : Form
    {
        private SvgDocument FSvgDoc;
        private SimplePsd.CPSD psd = new SimplePsd.CPSD();
        private bool ignoreDurChange = false;
        private string lastDir = "";

        public Form1()
        {
            InitializeComponent();
            renderingWindow.SetDefaults();
            SetDefaultTextBox();

            textFps.LostFocus += textFps_LostFocus;
            textDurMs.LostFocus += textDurMs_LostFocus;
            textFps.KeyPress += new KeyPressEventHandler(textFps_Enter);
            textDurMs.KeyPress += new KeyPressEventHandler(textDurMs_Enter);

            AllowDrop = true;
            DragEnter += new DragEventHandler(Form1_DragEnter);
            DragDrop += new DragEventHandler(Form1_DragDrop);

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textFps_Update()
        {
            if (ignoreDurChange) { return; }

            int val = 60;
            if (int.TryParse(textFps.Text, out val))
            {
                val = Math.Max(1, val);
                val = Math.Min(val, 1000);

                float ms = ((1.0f / (float)val) * 1000.0f);
                SetDuration(ms);
            }
        }

        private void textFps_TextChanged(object sender, EventArgs e) { }

        private void textFps_LostFocus(object sender, EventArgs e) { textFps_Update(); }

        private void textFps_Enter(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return) { textFps_Update(); }
        }

        private void textDurMs_Update()
        {
            if (ignoreDurChange) { return; }

            if (ignoreDurChange) { return; }

            float val = 16.67f;
            if (float.TryParse(textDurMs.Text, out val))
            {
                val = Math.Max(1, val);
                val = Math.Min(val, 10000);
                SetDuration(val);
            }
        }

        private void textDurMs_TextChanged(object sender, EventArgs e) { }

        private void textDurMs_LostFocus(object sender, EventArgs e) { textDurMs_Update(); }

        private void textDurMs_Enter(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return) { textDurMs_Update(); }
        }

        private void textColumns_TextChanged(object sender, EventArgs e)
        {
            int val = 1;
            if (int.TryParse(textColumns.Text, out val)) { renderingWindow.columns = Math.Max(1, val); }
        }

        private void textRows_TextChanged(object sender, EventArgs e)
        {
            int val = 1;
            if (int.TryParse(textRows.Text, out val)) { renderingWindow.rows = Math.Max(1, val); }
        }

        private void textHeight_TextChanged(object sender, EventArgs e)
        {
            int val = 1;
            if (int.TryParse(textHeight.Text, out val)) { renderingWindow.height = Math.Max(1, val); }
        }

        private void textWidth_TextChanged(object sender, EventArgs e)
        {
            int val = 1;
            if (int.TryParse(textWidth.Text, out val)) { renderingWindow.width = Math.Max(1, val); }
        }

        private void textZoom_TextChanged(object sender, EventArgs e)
        {
            float val = 1.0f;
            if (float.TryParse(textZoom.Text, out val))
            {
                renderingWindow.zoom = Math.Max(0.001f, val);
                renderingWindow.zoom = Math.Min(val, 50.0f);
            }
        }

        private void buttonStartStop_Click(object sender, EventArgs e)
        {
            renderingWindow.playing = !renderingWindow.playing;
            if (renderingWindow.playing) { buttonStartStop.Text = "Stop"; }
            else { buttonStartStop.Text = "Play"; }
        }

        void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
        }

        void Form1_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files.Length > 0)
            {
                LoadFile(files[0]);
            }
        }

        private void SetDefaultTextBox()
        {
            SetDuration(6 * 16.67f);
            textWidth.Text      =   renderingWindow.width.ToString();
            textHeight.Text     =   renderingWindow.height.ToString();
            textRows.Text       =   renderingWindow.rows.ToString();
            textColumns.Text    =   renderingWindow.columns.ToString();
            textHeight.Text     =   renderingWindow.height.ToString();
            textZoom.Text       =   renderingWindow.zoom.ToString();
            viewSheet.Checked   =   renderingWindow.viewSheet;
            if (renderingWindow.playing) { buttonStartStop.Text = "Stop"; }
            else { buttonStartStop.Text = "Play"; }
        }

        private void SetDuration(float ms)
        {
            renderingWindow.duration = Math.Max(1, ms);
            renderingWindow.duration = Math.Min(ms, 10000);
            //Console.WriteLine(ms);

            ignoreDurChange = true;
            textFps.Text = ((int)(1000.0f / 1.0f / renderingWindow.duration + 0.5f)).ToString();
            textDurMs.Text = renderingWindow.duration.ToString("N2");
            ignoreDurChange = false;
        }

        void LoadFile(string file)
        {
            string extension = file.Split('.')[1].ToUpper();
            if (extension == null || extension == "") { return; }

            if (extension == "GIF"  ||
                extension == "JPG"  ||
                extension == "JPEG" ||
                extension == "BMP"  ||
                extension == "WMF"  ||
                extension == "PNG")
            {
                LoadImage(file);
                SetDefaultTextBox();
            }
            else if (extension == "PSD")
            {
                LoadPsd(file);
                SetDefaultTextBox();
            }
            else if (extension == "SVG")
            {
                LoadSvg(file);
                SetDefaultTextBox();
            }
        }

        private void LoadImage(string file)
        {
            renderingWindow.SetTexture(file);
        }

        private void LoadSvg(string file)
        {
            FSvgDoc = SvgDocument.Open(file);
            Bitmap bmap = FSvgDoc.Draw();
            renderingWindow.SetTexture(bmap);
        }

        private void LoadPsd(string file)
        {
            int nResult = psd.Load(file);
            if (nResult == 0)
            {
                renderingWindow.SetTexture(Image.FromHbitmap(psd.GetHBitmap()));
            }
        }

        private void loadImage_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.InitialDirectory = lastDir == "" ? "C:\\" : lastDir;
                dialog.Filter =  "Image (*.gif;*.jpg;*.jpeg;*.png;*.psd;*.svg) | *.gif;*.jpg;*.jpeg;*.png;*.psd;*.svg";
                dialog.FilterIndex = 2;
                dialog.RestoreDirectory = true;

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    LoadFile(dialog.FileName);
                    lastDir = dialog.FileName;             
                }
            }
        }

        private void viewSheet_CheckedChanged(object sender, EventArgs e)
        {
            renderingWindow.viewSheet = viewSheet.Checked;
        }
    }
}
