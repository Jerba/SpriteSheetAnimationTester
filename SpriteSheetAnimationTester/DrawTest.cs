﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Forms.Controls;
using System.IO;
using System;
using System.Diagnostics;

namespace SpriteSheetAnimationTester
{
    public class DrawTest : DrawWindow
    {
        public int width { get; set; }
        public int height { get; set; }
        public int rows { get; set; }
        public int columns { get; set; }
        public float duration { get; set; }
        public float zoom { get; set; }
        public bool playing { get; set; }
        public bool viewSheet { get; set; }

        public int rowIndex;
        public int columnIndex;

        private Stopwatch watch;
        private long lastMs;

        public Color gridColor { get; set; }
        Texture2D texture   = null;
        Texture2D pixel     = null;

        protected override void Initialize()
        {
            base.Initialize();
            SetDefaults(false);

            watch = new Stopwatch();
            watch.Start();
            lastMs = watch.ElapsedMilliseconds;

            Editor.BackgroundColor = Color.White;
            gridColor = Color.Black;
            pixel = new Texture2D(Editor.graphics, 1, 1);
            pixel.SetData<Color>(new Color[] { Color.White });
        }

        protected override void Draw()
        {
            if (playing)
            {
                double ms = watch.ElapsedMilliseconds - lastMs;

                if (ms >= duration)
                {
                    //Console.WriteLine(ms + " target: " + duration);
                    lastMs = watch.ElapsedMilliseconds;
                    ++rowIndex;
                    //Console.WriteLine(rowIndex);
                    if (rowIndex >= rows || rowIndex * width >= texture.Width)
                    {
                        rowIndex = 0;
                        ++columnIndex;
                        if (columnIndex >= columns || columnIndex * height >= texture.Height)
                        {
                            columnIndex = 0;
                        }
                    }
                }
            }

            base.Draw();
            Editor.spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.NonPremultiplied, SamplerState.PointClamp);

            if (texture != null)
            {
                if (!viewSheet)
                {
                    Rectangle dest = new Rectangle(0, 0, (int)((float)width * zoom), (int)((float)height * zoom));
                    Rectangle src = new Rectangle(rowIndex * width, columnIndex * height, width, height);
                    Editor.spriteBatch.Draw(texture, dest, src, Color.White);
                }
                else
                {
                    Rectangle dest = new Rectangle(0, 0, texture.Width, texture.Height);
                    Rectangle src = new Rectangle(0, 0, texture.Width, texture.Height);
                    Editor.spriteBatch.Draw(texture, dest, src, Color.White);

                    for (int i = 0; i < rows; ++i)
                    {
                        Vector2 start = new Vector2(i * width, 0);
                        Vector2 end   = new Vector2(i * width, texture.Height);
                        DrawLine(start, end);
                    }
                    for (int i = 0; i < columns; ++i)
                    {
                        Vector2 start = new Vector2(0, i * height);
                        Vector2 end   = new Vector2(texture.Width, i * height);
                        DrawLine(start, end);
                    }
                }

                //src = ClampInsideTextureDimensions(src);
                
            }

            Editor.spriteBatch.End();
        }

        void DrawLine(Vector2 point1, Vector2 point2)
        {
            float angle = (float)Math.Atan2(point2.Y - point1.Y, point2.X - point1.X);
            float length = (point2 - point1).Length();

            Editor.spriteBatch.Draw(pixel, point1, null, gridColor, angle, Vector2.Zero, new Vector2(length, 1.0f), SpriteEffects.None, 1);
        }

        Rectangle ClampInsideTextureDimensions(Rectangle rect)
        {
            Rectangle ret;
            ret.X = Math.Max(rect.X, 0);
            ret.Y = Math.Max(rect.Y, 0);
            ret.X = Math.Min(texture.Width - 2, ret.X);
            ret.Y = Math.Min(texture.Height - 2, ret.Y);
            ret.Width = Math.Min(rect.X + rect.Width, texture.Width) - ret.X;
            ret.Height = Math.Min(rect.Y + rect.Height, texture.Height) - ret.Y;
            return ret;
        }

        public void SetTexture(System.Drawing.Bitmap bitmap)
        {
            if (texture != null) { texture.Dispose(); }
	        int bufferSize = bitmap.Height * bitmap.Width * 4;
	
 	        System.IO.MemoryStream memoryStream = new System.IO.MemoryStream(bufferSize);
 	        bitmap.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);
 	        texture = Texture2D.FromStream(Editor.graphics, memoryStream);
            SetDefaults(true);
        }

        public void SetTexture(string file)
        {
            if (texture != null) { texture.Dispose(); }
            FileStream fileStream = new FileStream(file, FileMode.Open);
            texture = Texture2D.FromStream(Editor.graphics, fileStream);
            fileStream.Dispose();
            SetDefaults(true);
        }

        public void SetDefaults(bool toImageSize = false)
        {
            rowIndex    = 0;
            columnIndex = 0;
            width       = toImageSize && texture != null ? texture.Width : 1;
            height      = toImageSize && texture != null ? texture.Height : 1;
            rows        = 1;
            columns     = 1;
            zoom        = 1.0f;
            playing     = false;
            viewSheet   = false;
        }
    }
}
